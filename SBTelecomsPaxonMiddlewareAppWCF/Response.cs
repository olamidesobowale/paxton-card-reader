﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace SBTelecomsPaxonMiddlewareAppWCF
{
    [DataContract]
    [KnownType(typeof(User))]
    [KnownType(typeof(List<User>))]
    [KnownType(typeof(List<int>))]
    [KnownType(typeof(CheckInCheckOut))]
    [KnownType(typeof(List<CheckInCheckOut>))]
    [KnownType(typeof(ListCheckInCheckOut))]
    [KnownType(typeof(CheckInCheckOutACU))]
    public class Response
    {
        [DataMember]
        public long Code { get; set; }
        [DataMember]
        public object Data { get; set; }
        [DataMember]
        public string Description { get; set; }
    }
  public  static class ResponseGenerator
    {
        public static Response OK(object data)
        {
            return new Response
            {
                Code = 200,
                Data = data,
                Description = "Successful"
        };
        }
        public static Response OK()
        {
            return new Response
            {
                Code = 200,
                Data = null,
                Description = "No Content"
            };
        }
        public static Response Validation(string errorMessage)
        {
            return new Response
            {
                Code = 201,
                Data = null,
                Description = errorMessage
            };
        }
    }
}
