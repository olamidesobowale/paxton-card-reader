﻿using Paxton.Net2.OemClientLibrary;
using SBTelecomsPaxonMiddlewareAppWCF;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace SBTelecomsPaxonMiddlewareAppWCF
{
   public static class Service
    {
        public static Response GetAllUsers()
        {
            var result =Net2ClientObject.oemClient.ViewUserRecords().UsersList().Values.Select(x => { return User.getUser(x); }).ToList();
            return result.Count == 0 ? ResponseGenerator.OK() : ResponseGenerator.OK(result);
        }
        public static Response getByFilter(string filtertype, string filtervalue)
        {
            var result= Net2ClientObject.oemClient.ViewUserRecords($"where {filtertype}='{filtervalue}'").UsersList().Values.Select(x => { return User.getUser(x); }).ToList();
            return result.Count == 0 ? ResponseGenerator.OK() : ResponseGenerator.OK(result);
        }
        public static Response GetUserByUserId(string userid)
        {
            var obj = Net2ClientObject.oemClient.ViewUserRecords().UsersList().Where(x => x.Value.UserId == Convert.ToInt32(userid));
            if (obj.Count() == 0)
                return ResponseGenerator.Validation("Invalid user id");
            var result = User.getUser(obj.First().Value);
            return ResponseGenerator.OK(result);
            
        }
        public static Response GetUserByAccessToken(string Token)
        {
            var result = Net2ClientObject.oemClient.QueryDb($"Select UserID from Cards where CardNumber='{Token}'");
            if (result.Tables.Count > 0 && result.Tables[0].Rows[0] != null)
            {
                var userid = result.Tables[0].Rows[0][0];
                var obj = Net2ClientObject.oemClient.ViewUserRecords().UsersList().Where(x => x.Value.UserId == Convert.ToInt32(userid));
                if (obj.Count() == 0)
                    return ResponseGenerator.Validation("The user attached to this token is no longer valid");
                var data = User.getUser(obj.First().Value);
                return ResponseGenerator.OK(data);
            }
            else
            {
                return ResponseGenerator.Validation("The card token is not registered with a user");
            }
        }
        public static Response GetCardTokenByUserId(string userid)
        {
            var result= Net2ClientObject.oemClient.ViewCards(Convert.ToInt32(userid));
            return result.Count == 0 ? ResponseGenerator.OK() : ResponseGenerator.OK(result);
        }
        public static Response GetCheckInCheckOutToday(string type, string userid)
        {
            return GetCheckInCheckOut(type, userid, DateTime.Now.Date.ToString("dd-MM-yyyy"));

        }
        public static Response GetCheckInCheckOut(string type, string userid, string date)
        {
            date = DateTime.ParseExact(date,"dd-MM-yyyy", CultureInfo.InvariantCulture).ToString("dd/MM/yyyy");
            User user = null;
            CheckInCheckOut checkInCheckOut = new CheckInCheckOut();
            if (type.ToLower() == "cardid")
            {
                var result = Net2ClientObject.oemClient.QueryDb($"Select UserID from Cards where CardNumber='{userid}'");
                if (result.Tables.Count > 0 && result.Tables[0].Rows[0] != null)
                {
                    var id = result.Tables[0].Rows[0][0];
                    var obj = Net2ClientObject.oemClient.ViewUserRecords().UsersList().Where(x => x.Value.UserId == Convert.ToInt32(id));
                    if (obj.Count() == 0)
                        return ResponseGenerator.Validation("The user attached to this token is no longer valid");
                    user = User.getUser(obj.First().Value);

                }

            }
            else if(type.ToLower()=="user")
            {
                var obj = Net2ClientObject.oemClient.ViewUserRecords().UsersList().Where(x => x.Value.UserId == Convert.ToInt32(userid));
                if (obj.Count() == 0)
                    return ResponseGenerator.Validation("Invalid user id");
                user = User.getUser(obj.First().Value);
            }
            IEnumerable<IEventView> respCheckIn = null;
            IEnumerable<IEventView> respCheckOut = null;
            var respCheck = Net2ClientObject.oemClient.ViewEvents(int.MaxValue, "asc").EventsList().Values.Where(x => x.UserId == user.UserId && x.EventDate == date && x.EventType == 20).ToList().AsQueryable();
            Expression ExIn = null;
            Expression ExOut = null;
            var param=Expression.Parameter(typeof(IEventView), "x");
            foreach (var acu in Net2ClientObject.acuaddress)
            {
                if (ExIn == null)
                {
                    ExIn = Expression.Equal(Expression.Property(param, typeof(IEventView).GetProperty("DeviceName")),Expression.Constant($"ACU:{acu} (In)"));
                    ExOut = Expression.Equal(Expression.Property(param, typeof(IEventView).GetProperty("DeviceName")),Expression.Constant($"ACU:{acu} (Out)") );
                }
                else
                {
                    ExIn = Expression.OrElse(ExIn, Expression.Equal(Expression.Property(param, typeof(IEventView).GetProperty("DeviceName")), Expression.Constant($"ACU:{acu} (In)")));
                    ExOut = Expression.OrElse(ExOut, Expression.Equal(Expression.Property(param, typeof(IEventView).GetProperty("DeviceName")), Expression.Constant($"ACU:{acu} (Out)")));
                }

            }
            MethodCallExpression whereCallExpressionIn = Expression.Call(
    typeof(Queryable),
    "Where",
    new Type[] { respCheck.ElementType },
    respCheck.Expression,
    Expression.Lambda<Func<IEventView, bool>>(ExIn, new ParameterExpression[] { param }));
            MethodCallExpression whereCallExpressionOut = Expression.Call(
  typeof(Queryable),
  "Where",
  new Type[] { respCheck.ElementType },
  respCheck.Expression,
  Expression.Lambda<Func<IEventView, bool>>(ExOut, new ParameterExpression[] { param }));
            //foreach (var acu in Net2ClientObject.acuaddress) {
            //    respCheckIn = respCheck.Where(x => x.DeviceName == $"ACU:{acu} (In)");
            //    respCheckOut= respCheck.Where(x => x.DeviceName == $"ACU:{acu} (Out)");
            //}
            respCheckIn = respCheck.Provider.CreateQuery<IEventView>(whereCallExpressionIn).ToList();
            respCheckOut = respCheck.Provider.CreateQuery<IEventView>(whereCallExpressionOut).ToList();
            if (respCheckIn.Count() > 0)
                checkInCheckOut.CheckInTime = respCheckIn.First().EventTime;

            respCheckOut = respCheckOut.OrderBy(x => x.EventId);
            if (respCheckOut.Count() > 0)
                checkInCheckOut.CheckOutTime = respCheckOut.Last().EventTime;
            return ResponseGenerator.OK(checkInCheckOut);
        }
        public static Response GetCheckIn(string type, string userid, string date)
        {
            return GetCheckInCheckOut(type, userid,date);
        }
        public static Response GetCheckOut(string type, string userid, string date)
        {
            return GetCheckInCheckOut(type, userid, date);
        }
        public static Response GetLogForACU(string type, string acu, string userid, string date)
        {
            date = DateTime.ParseExact(date, "dd-MM-yyyy", CultureInfo.InvariantCulture).ToString("dd/MM/yyyy");
            User user = null;
            var checkInCheckOut = new CheckInCheckOutACU();
            if (type.ToLower() == "cardid")
            {
                var result = Net2ClientObject.oemClient.QueryDb($"Select UserID from Cards where CardNumber='{userid}'");
                if (result.Tables.Count > 0 && result.Tables[0].Rows[0] != null)
                {
                    var id = result.Tables[0].Rows[0][0];
                    var obj = Net2ClientObject.oemClient.ViewUserRecords().UsersList().Where(x => x.Value.UserId == Convert.ToInt32(id));
                    if (obj.Count() == 0)
                        return ResponseGenerator.Validation("The user attached to this token is no longer valid");
                    user = User.getUser(obj.First().Value);

                }

            }
            else if (type.ToLower() == "user")
            {
                var obj = Net2ClientObject.oemClient.ViewUserRecords().UsersList().Where(x => x.Value.UserId == Convert.ToInt32(userid));
                if (obj.Count() == 0)
                    return ResponseGenerator.Validation("Invalid user id");
                user = User.getUser(obj.First().Value);
            }
            IEnumerable<IEventView> respCheckIn = null;
            IEnumerable<IEventView> respCheckOut = null;
            var respCheck = Net2ClientObject.oemClient.ViewEvents(int.MaxValue, "asc").EventsList().Values.Where(x => x.UserId == user.UserId && x.EventDate == date && x.EventType == 20).ToList().AsQueryable();
            Expression ExIn = null;
            Expression ExOut = null;
            var param = Expression.Parameter(typeof(IEventView), "x");
           
                    ExIn = Expression.Equal(Expression.Property(param, typeof(IEventView).GetProperty("DeviceName")), Expression.Constant($"ACU:{acu} (In)"));
                    ExOut = Expression.Equal(Expression.Property(param, typeof(IEventView).GetProperty("DeviceName")), Expression.Constant($"ACU:{acu} (Out)"));
               
            MethodCallExpression whereCallExpressionIn = Expression.Call(
    typeof(Queryable),
    "Where",
    new Type[] { respCheck.ElementType },
    respCheck.Expression,
    Expression.Lambda<Func<IEventView, bool>>(ExIn, new ParameterExpression[] { param }));
            MethodCallExpression whereCallExpressionOut = Expression.Call(
  typeof(Queryable),
  "Where",
  new Type[] { respCheck.ElementType },
  respCheck.Expression,
  Expression.Lambda<Func<IEventView, bool>>(ExOut, new ParameterExpression[] { param }));
    
            respCheckIn = respCheck.Provider.CreateQuery<IEventView>(whereCallExpressionIn).ToList();
            respCheckOut = respCheck.Provider.CreateQuery<IEventView>(whereCallExpressionOut).ToList();
            if (respCheckIn.Count() > 0)
                checkInCheckOut.CheckIn = respCheckIn.Select(x=>x.EventTime).ToArray();

            respCheckOut = respCheckOut.OrderBy(x => x.EventId);
            if (respCheckOut.Count() > 0)
                checkInCheckOut.CheckOut= respCheckOut.Select(x => x.EventTime).ToArray();
            checkInCheckOut.ACU = acu;
            return ResponseGenerator.OK(checkInCheckOut);
        }
        public static Response GetLogForAllACU(string type, string userid, string date)
        {
            date = DateTime.ParseExact(date, "dd-MM-yyyy", CultureInfo.InvariantCulture).ToString("dd/MM/yyyy");
            User user = null;
            var checkInCheckOut = new ListCheckInCheckOut();
            if (type.ToLower() == "cardid")
            {
                var result = Net2ClientObject.oemClient.QueryDb($"Select UserID from Cards where CardNumber='{userid}'");
                if (result.Tables.Count > 0 && result.Tables[0].Rows[0] != null)
                {
                    var id = result.Tables[0].Rows[0][0];
                    var obj = Net2ClientObject.oemClient.ViewUserRecords().UsersList().Where(x => x.Value.UserId == Convert.ToInt32(id));
                    if (obj.Count() == 0)
                        return ResponseGenerator.Validation("The user attached to this token is no longer valid");
                    user = User.getUser(obj.First().Value);

                }

            }
            else if (type.ToLower() == "user")
            {
                var obj = Net2ClientObject.oemClient.ViewUserRecords().UsersList().Where(x => x.Value.UserId == Convert.ToInt32(userid));
                if (obj.Count() == 0)
                    return ResponseGenerator.Validation("Invalid user id");
                user = User.getUser(obj.First().Value);
            }
            
            var respCheck = Net2ClientObject.oemClient.ViewEvents(int.MaxValue, "asc").EventsList().Values.Where(x => x.UserId == user.UserId && x.EventDate == date && x.EventType == 20).ToList().AsQueryable();



            var groupings = respCheck.GroupBy<IEventView, String>(x => x.DeviceName.Substring(0, 11));
            foreach (var item in groupings)
            {

                checkInCheckOut.List.Add(new CheckInCheckOutACU { ACU = item.Key.Substring(4), CheckIn = item.Where(x => x.DeviceName == $"{item.Key} (In)").Select(x => x.EventTime).ToArray(), CheckOut = item.Where(x => x.DeviceName == $"{item.Key} (Out)").Select(x => x.EventTime).ToArray() });

            }
            return ResponseGenerator.OK(checkInCheckOut);
        }
       public static Response GetLogforEntrance(string type, string userid, string date)
        {
            date = DateTime.ParseExact(date, "dd-MM-yyyy", CultureInfo.InvariantCulture).ToString("dd/MM/yyyy");
            User user = null;
            var checkInCheckOut = new ListCheckInCheckOut();
            if (type.ToLower() == "cardid")
            {
                var result = Net2ClientObject.oemClient.QueryDb($"Select UserID from Cards where CardNumber='{userid}'");
                if (result.Tables.Count > 0 && result.Tables[0].Rows[0] != null)
                {
                    var id = result.Tables[0].Rows[0][0];
                    var obj = Net2ClientObject.oemClient.ViewUserRecords().UsersList().Where(x => x.Value.UserId == Convert.ToInt32(id));
                    if (obj.Count() == 0)
                        return ResponseGenerator.Validation("The user attached to this token is no longer valid");
                    user = User.getUser(obj.First().Value);

                }

            }
            else if (type.ToLower() == "user")
            {
                var obj = Net2ClientObject.oemClient.ViewUserRecords().UsersList().Where(x => x.Value.UserId == Convert.ToInt32(userid));
                if (obj.Count() == 0)
                    return ResponseGenerator.Validation("Invalid user id");
                user = User.getUser(obj.First().Value);
            }
            IEnumerable<IEventView> respCheckIn = null;
            IEnumerable<IEventView> respCheckOut = null;
            var respCheck = Net2ClientObject.oemClient.ViewEvents(int.MaxValue, "asc").EventsList().Values.Where(x => x.UserId == user.UserId && x.EventDate == date && x.EventType == 20).ToList().AsQueryable();
            Expression ExIn = null;
            Expression ExOut = null;
            var param = Expression.Parameter(typeof(IEventView), "x");
            foreach (var acu in Net2ClientObject.acuaddress)
            {
                if (ExIn == null)
                {
                    ExIn = Expression.Equal(Expression.Property(param, typeof(IEventView).GetProperty("DeviceName")), Expression.Constant($"ACU:{acu} (In)"));
                    ExOut = Expression.Equal(Expression.Property(param, typeof(IEventView).GetProperty("DeviceName")), Expression.Constant($"ACU:{acu} (Out)"));
                }
                else
                {
                    ExIn = Expression.OrElse(ExIn, Expression.Equal(Expression.Property(param, typeof(IEventView).GetProperty("DeviceName")), Expression.Constant($"ACU:{acu} (In)")));
                    ExOut = Expression.OrElse(ExOut, Expression.Equal(Expression.Property(param, typeof(IEventView).GetProperty("DeviceName")), Expression.Constant($"ACU:{acu} (Out)")));
                }

            }
            MethodCallExpression whereCallExpressionIn = Expression.Call(
    typeof(Queryable),
    "Where",
    new Type[] { respCheck.ElementType },
    respCheck.Expression,
    Expression.Lambda<Func<IEventView, bool>>(ExIn, new ParameterExpression[] { param }));
            MethodCallExpression whereCallExpressionOut = Expression.Call(
  typeof(Queryable),
  "Where",
  new Type[] { respCheck.ElementType },
  respCheck.Expression,
  Expression.Lambda<Func<IEventView, bool>>(ExOut, new ParameterExpression[] { param }));
            //foreach (var acu in Net2ClientObject.acuaddress) {
            //    respCheckIn = respCheck.Where(x => x.DeviceName == $"ACU:{acu} (In)");
            //    respCheckOut= respCheck.Where(x => x.DeviceName == $"ACU:{acu} (Out)");
            //}
            respCheckIn = respCheck.Provider.CreateQuery<IEventView>(whereCallExpressionIn).ToList();
            respCheckOut = respCheck.Provider.CreateQuery<IEventView>(whereCallExpressionOut).ToList();
            foreach (var ac in Net2ClientObject.acuaddress) {
                checkInCheckOut.List.Add(new CheckInCheckOutACU
                {
                    ACU = ac,
                    CheckIn = respCheckIn.Where(x => x.DeviceName == $"ACU:{ac} (In)").Select(x=>x.EventTime).ToArray(),
                    CheckOut = respCheckIn.Where(x => x.DeviceName == $"ACU:{ac} (Out)").Select(x => x.EventTime).ToArray()
            });
            }
            return ResponseGenerator.OK(checkInCheckOut);
        }
    }
}
