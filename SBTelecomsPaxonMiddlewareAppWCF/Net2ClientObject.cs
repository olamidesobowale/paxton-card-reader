﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Paxton.Net2.OemClientLibrary;
using Newtonsoft.Json;
using System.Drawing;
using System.Runtime.Serialization;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Timers;

namespace SBTelecomsPaxonMiddlewareAppWCF
{
    public  class Net2ClientObject
    {
        public static OemClient oemClient;
        public static string[] acuaddress;
        public static Timer UpdateUserTimer;
        public static void Initialize()
        {
            string userName = ConfigurationSettings.AppSettings.Get("username");
            string password = ConfigurationSettings.AppSettings.Get("password");
            string host = ConfigurationSettings.AppSettings.Get("host");
            string port = ConfigurationSettings.AppSettings.Get("port");
           acuaddress= ConfigurationSettings.AppSettings.Get("EntranceACU").Split(',');
            Console.WriteLine($"username {userName} , password {password}, host {host}, port {port}");
            UpdateUserTimer = new Timer();
            UpdateUserTimer.AutoReset = true;
            UpdateUserTimer.Enabled = true;
            UpdateUserTimer.Interval = 8000;
            UpdateUserTimer.Elapsed += (x, y) => {
                int lastId = Convert.ToInt32(ConfigurationSettings.AppSettings.Get("idoflastaddeduser"));
               var newUsers= oemClient.ViewUserRecords().UsersList().Where(v => v.Key > lastId).Select(t => t.Value);
                
                if (newUsers.Count() > 0)
                {
                    int newlastId = newUsers.OrderBy(c => c.UserId).Last().UserId;
                    //makerequest
                    string url = ConfigurationSettings.AppSettings.Get("userupdateurl");
                    var body = new
                    {
                        Users=newUsers.ToList()
                    };

                    HttpClient client = new HttpClient();
                    client.PostAsync(url, body, new JsonMediaTypeFormatter()).ContinueWith(z =>
                    {
                        var httpResponseMessage = z.Result;
                    });

                }

            };
            UpdateUserTimer.Start();
           
            oemClient = new OemClient(host, Convert.ToInt32(port));


          
            if (oemClient.CanAuthenticate)
            {
                AuthenticateUser(userName, password);
                oemClient.Net2AccessEvent += (x, y) =>
                {
                    if (y.EventType == 20) {
                        if (y.UserId != 0) {
                       
                            string url = ConfigurationSettings.AppSettings.Get("eventupdateurl");
                            var body = new
                            {
                                UserId = y.UserId,
                                Date = y.EventDate,
                                Time = y.EventTime,
                                Type = y.DeviceName[13] == 'I' ? "In" : "Out",
                                ACU = y.DeviceName.Substring(4, 7)
                            };
                            
                            HttpClient client = new HttpClient();
                            client.PostAsync(url, body, new JsonMediaTypeFormatter()).ContinueWith( z =>
                              {
                                  var httpResponseMessage = z.Result;
                              });
                            }
                    }
                };
           
    
              
            }
            Console.WriteLine(DateTime.Now.Date.ToString("yyyy-MM-dd"));
          Console.WriteLine(JsonConvert.SerializeObject( oemClient.ViewEvents(int.MaxValue,"asc").EventsList().Where(x=>x.Value.EventDate== "29/03/2018" && x.Value.UserId==1&&x.Value.EventType==20).ToList()));
            Console.WriteLine(JsonConvert.SerializeObject(oemClient.QueryDb($"select * from EventsEx  where UserID=1 and EventTime='2018-03-29 00:00:00'")));

            
        }
        private static bool AuthenticateUser(string userName, string password)
        {
            IOperators operators = oemClient.GetListOfOperators();
            var operatorsList = operators.UsersDictionary();
            try
            {
                foreach (int userID in operatorsList.Keys)
                {
                    if (operatorsList[userID] == userName)
                    {
                        var result = oemClient.AuthenticateUser(userID, password);
                       
                    }
                }
            }
            catch (Exception)
            {

                return true;
            }
           
            return true;
        }
        
       

    }
    
    public class User
    {

        public string AccessLevel { get; set; }


        public int AccessLevelId { get; set; }

        public string ActivationDate { get; set; }

        public bool Active { get; set; }

        public string Department { get; set; }

        public int DepartmentId { get; set; }

        public string DisplayName { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string Surname { get; set; }

        public int UserId { get; set; }

        public int LastAreaId { get; set; }

        public string LastArea { get; set; }

        public string LastAccessTime
        {
            get; set;
        }

        public bool AntiPassbackUser { get; set; }

        public bool AlarmUser { get; set; }

        public string Picture { get; set; }

        public string PIN { get; set; }

        public string Telephone { get; set; }

        public string Extension { get; set; }

        public string ExpiryDate
        {
            get; set;
        }

        public string Fax { get; set; }

        public string Field1_100 { get; set; }

        public string Field2_100 { get; set; }

        public string Field3_50 { get; set; }

        public string Field4_50 { get; set; }

        public string Field5_50 { get; set; }

        public string Field6_50 { get; set; }

        public string Field7_50 { get; set; }

        public string Field8_50 { get; set; }

        public string Field9_50 { get; set; }

        public string Field10_50 { get; set; }

        public string Field11_50 { get; set; }

        public string Field12_50 { get; set; }

        public string Field13_Memo { get; set; }

        public string Field14_50 { get; set; }

        public System.Drawing.Image UserImage { get; set; }

        public Guid UserGuid { get; set; }

        public string LastUpdated
        {
            get; set;
        }

        public bool Global { get; set; }
        public static User getUser(IUserView userView)
        {
            return new User
            {

                AccessLevel = userView.AccessLevel,


                AccessLevelId = userView.AccessLevelId,

                ActivationDate = userView.ActivationDate.ToLongDateString(),

                Active = userView.Active,

                Department = userView.Department,

                DepartmentId = userView.DepartmentId,

                DisplayName = userView.DisplayName,

                FirstName = userView.FirstName,

                MiddleName = userView.MiddleName,

                Surname = userView.Surname,

                UserId = userView.UserId,

                LastAreaId = userView.LastAreaId,

                LastArea = userView.LastArea,

                LastAccessTime = userView.LastAccessTime.ToLongDateString(),

                AntiPassbackUser = userView.AntiPassbackUser,

                AlarmUser = userView.AlarmUser,

                Picture = userView.Picture,

                PIN = userView.PIN,
                Telephone = userView.Telephone,

                Extension = userView.Extension,

                ExpiryDate = userView.ExpiryDate.ToLongDateString(),

                Fax = userView.Fax,

                Field1_100 = userView.Field1_100,

                Field2_100 = userView.Field2_100,

                Field3_50 = userView.Field3_50,

                Field4_50 = userView.Field4_50,

                Field5_50 = userView.Field5_50,

                Field6_50 = userView.Field6_50,

                Field7_50 = userView.Field7_50,

                Field8_50 = userView.Field8_50,

                Field9_50 = userView.Field9_50,

                Field10_50 = userView.Field10_50,

                Field11_50 = userView.Field11_50,

                Field12_50 = userView.Field12_50,

                Field13_Memo = userView.Field13_Memo,

                Field14_50 = userView.Field14_50,

                UserImage = userView.UserImage,

                UserGuid = userView.UserGuid,

                LastUpdated = userView.LastUpdated.ToLongDateString(),

                Global = userView.Global

            };
        }
    }
    public class CheckInCheckOut
    {
        public string CheckInTime { get; set; }
        public string CheckOutTime { get; set; }
    }
    public class ListCheckInCheckOut
    {
     public List<CheckInCheckOutACU> List { get; set; }
        public ListCheckInCheckOut()
        {
            List = new List<CheckInCheckOutACU>(); 
        }
    }
    public class CheckInCheckOutACU
    {
        public string ACU { get; set; }
        public String[] CheckIn { get; set; }
        public String[] CheckOut { get; set; }
    }
    //[DataContract]
    //public class User 
    //{
    //    [DataMember]
    //    public string AccessLevel { get; set; }
    //    [DataMember]

    //    public int AccessLevelId { get; set; }
    //    [DataMember]
    //    public string ActivationDate { get; set; }
    //    [DataMember]
    //    public bool Active { get; set; }
    //    [DataMember]
    //    public string Department { get; set; }
    //    [DataMember]
    //    public int DepartmentId { get; set; }
    //    [DataMember]
    //    public string DisplayName { get; set; }
    //    [DataMember]
    //    public string FirstName { get; set; }
    //    [DataMember]
    //    public string MiddleName { get; set; }
    //    [DataMember]
    //    public string Surname { get; set; }
    //    [DataMember]
    //    public int UserId { get; set; }
    //    [DataMember]
    //    public int LastAreaId { get; set; }
    //    [DataMember]
    //    public string LastArea { get; set; }
    //    [DataMember]
    //    public string LastAccessTime {
    //        get;set;
    //    }
    //    [DataMember]
    //    public bool AntiPassbackUser { get; set; }
    //    [DataMember]
    //    public bool AlarmUser { get; set; }
    //    [DataMember]
    //    public string Picture { get; set; }
    //    [DataMember]
    //    public string PIN { get; set; }
    //    [DataMember]
    //    public string Telephone { get; set; }
    //    [DataMember]
    //    public string Extension { get; set; }
    //    [DataMember]
    //    public string ExpiryDate {
    //        get;set;
    //    }
    //    [DataMember]
    //    public string Fax { get; set; }
    //    [DataMember]
    //    public string Field1_100 { get; set; }
    //    [DataMember]
    //    public string Field2_100 { get; set; }
    //    [DataMember]
    //    public string Field3_50 { get; set; }
    //    [DataMember]
    //    public string Field4_50 { get; set; }
    //    [DataMember]
    //    public string Field5_50 { get; set; }
    //    [DataMember]
    //    public string Field6_50 { get; set; }
    //    [DataMember]
    //    public string Field7_50 { get; set; }
    //    [DataMember]
    //    public string Field8_50 { get; set; }
    //    [DataMember]
    //    public string Field9_50 { get; set; }
    //    [DataMember]
    //    public string Field10_50 { get; set; }
    //    [DataMember]
    //    public string Field11_50 { get; set; }
    //    [DataMember]
    //    public string Field12_50 { get; set; }
    //    [DataMember]
    //    public string Field13_Memo { get; set; }
    //    [DataMember]
    //    public string Field14_50 { get; set; }
    //    [DataMember]
    //    public System.Drawing.Image UserImage { get; set; }
    //    [DataMember]
    //    public Guid UserGuid { get; set; }
    //    [DataMember]
    //    public string LastUpdated {get;set;
    //            }
    //    [DataMember]
    //    public bool Global { get; set; }
    //    public static  User getUser(IUserView userView)
    //    {
    //        return new User
    //        {

    //            AccessLevel = userView.AccessLevel,


    //            AccessLevelId = userView.AccessLevelId,

    //            ActivationDate = userView.ActivationDate.ToLongDateString(),

    //            Active = userView.Active,

    //            Department = userView.Department,

    //            DepartmentId = userView.DepartmentId,

    //            DisplayName = userView.DisplayName,

    //            FirstName = userView.FirstName,

    //            MiddleName = userView.MiddleName,

    //            Surname = userView.Surname,

    //            UserId = userView.UserId,

    //            LastAreaId = userView.LastAreaId,

    //            LastArea = userView.LastArea,

    //          LastAccessTime = userView.LastAccessTime.ToLongDateString(),

    //            AntiPassbackUser = userView.AntiPassbackUser,

    //            AlarmUser = userView.AlarmUser,

    //            Picture = userView.Picture,

    //            PIN = userView.PIN,
    //            Telephone = userView.Telephone,

    //            Extension = userView.Extension,

    //            ExpiryDate = userView.ExpiryDate.ToLongDateString(),

    //            Fax = userView.Fax,

    //            Field1_100 = userView.Field1_100,

    //            Field2_100 = userView.Field2_100,

    //            Field3_50 = userView.Field3_50,

    //            Field4_50 = userView.Field4_50,

    //            Field5_50 = userView.Field5_50,

    //            Field6_50 = userView.Field6_50,

    //            Field7_50 = userView.Field7_50,

    //            Field8_50 = userView.Field8_50,

    //            Field9_50 = userView.Field9_50,

    //            Field10_50 = userView.Field10_50,

    //            Field11_50 = userView.Field11_50,

    //            Field12_50 = userView.Field12_50,

    //            Field13_Memo = userView.Field13_Memo,

    //            Field14_50 = userView.Field14_50,

    //            UserImage = userView.UserImage,

    //            UserGuid = userView.UserGuid,

    //            LastUpdated = userView.LastUpdated.ToLongDateString(),

    //            Global = userView.Global

    //        };
    //    }
    //}
}
