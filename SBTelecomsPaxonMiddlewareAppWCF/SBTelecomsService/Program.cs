﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using SBTelecomsPaxonMiddlewareAppWCF;

namespace SBTelecomsService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            Net2ClientObject.Initialize();
#if DEBUG
            SBTelecomsService sBTelecomsService = new SBTelecomsService();
            sBTelecomsService.OnDebug();
            Thread.Sleep(Timeout.Infinite);
#else
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new SBTelecomsService()
            };
            ServiceBase.Run(ServicesToRun);
#endif
        }
    }
}
