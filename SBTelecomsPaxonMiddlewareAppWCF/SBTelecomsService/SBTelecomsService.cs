﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using SBTelecomsPaxonMiddlewareAppWCF;

namespace SBTelecomsService
{
    partial class SBTelecomsService : ServiceBase
    {
        ServiceHost serviceHost = null;
        public SBTelecomsService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            // TODO: Add code here to start your service.
            serviceHost = new ServiceHost(typeof(SBTelecomsACUService));
            serviceHost.Open();
        }
        public void OnDebug()
        {
            OnStart(null);
        }

        protected override void OnStop()
        {
            // TODO: Add code here to perform any tear-down necessary to stop your service.
            if (serviceHost != null)
            {
                serviceHost.Close();
                serviceHost = null;
            }
        }
    }
}
