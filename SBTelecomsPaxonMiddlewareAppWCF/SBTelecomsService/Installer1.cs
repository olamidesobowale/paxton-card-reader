﻿using System;
using System.ComponentModel;
using System.ServiceProcess;
using System.Configuration.Install;

namespace SBTelecomsService
{
    [RunInstaller(true)]
    public partial class SBTelecomsServiceInstaller : Installer
    {
        ServiceProcessInstaller ServiceProcessInstaller;
        ServiceInstaller ServiceInstaller;
        public SBTelecomsServiceInstaller()
        {
            ServiceProcessInstaller = new ServiceProcessInstaller();
            ServiceProcessInstaller.Account = ServiceAccount.LocalSystem;
            ServiceInstaller = new ServiceInstaller();
            ServiceInstaller.ServiceName = "SBTelecomsCustomNet2Client";
            ServiceInstaller.Description = "rest api to communicate with net2 access control server";
            ServiceInstaller.DelayedAutoStart = true;
            Installers.Add(ServiceInstaller);
            Installers.Add(ServiceProcessInstaller);
        }
    }
}
