﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace SBTelecomsPaxonMiddlewareAppWCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface ISBTelecomsACUService
    {
        [OperationContract]
        [WebInvoke(Method = "GET",
       UriTemplate = "users", ResponseFormat = WebMessageFormat.Json)]
        Response getAllUsers();
        [OperationContract]
        [WebInvoke(Method = "GET",
       UriTemplate = "users/{filtertype}/{filtervalue}", ResponseFormat = WebMessageFormat.Json)]
        Response getByFilter(string filtertype, string filtervalue);
        [OperationContract]
        [WebInvoke(Method = "GET",
       UriTemplate = "users/getuserbyid/{userid}", ResponseFormat = WebMessageFormat.Json)]
        Response GetUserByUserId(string userid);
        [OperationContract]
        [WebInvoke(Method = "GET",
       UriTemplate = "users/getuserbyaccesstoken/{token}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedResponse)]
        Response GetUserByAccessToken(string Token);
        [OperationContract]
        [WebInvoke(Method = "GET",
       UriTemplate = "users/getallcardtokenforuser/{userid}", ResponseFormat = WebMessageFormat.Json)]
        Response GetCardTokenByUserId(string userid);
        [OperationContract]
        [WebInvoke(Method = "GET",
UriTemplate = "Access/GetCheckInCheckOutToday/{type}/{userid}", ResponseFormat = WebMessageFormat.Json)]
        Response GetCheckInCheckOutToday(string type, string userid);

        [OperationContract]
        [WebInvoke(Method = "GET",
UriTemplate = "Access/GetCheckInCheckOut/{type}/{date}/{userid}", ResponseFormat = WebMessageFormat.Json)]
        Response GetCheckInCheckOut(string type, string userid, string date);

        [OperationContract]
        [WebInvoke(Method = "GET",
UriTemplate = "Access/GetCheckInToday/{type}/{userid}", ResponseFormat = WebMessageFormat.Json)]
        Response GetCheckInToday(string type, string userid);
        [OperationContract]
        [WebInvoke(Method = "GET",
UriTemplate = "Access/GetCheckoutToday/{type}/{userid}", ResponseFormat = WebMessageFormat.Json)]
        Response GetCheckoutToday(string type, string userid);

        [OperationContract]
        [WebInvoke(Method = "GET",
UriTemplate = "Access/GetCheckIn/{type}/{date}/{userid}", ResponseFormat = WebMessageFormat.Json)]
        Response GetCheckIn(string type, string userid, string date);
        [OperationContract]
        [WebInvoke(Method = "GET",
UriTemplate = "Access/GetCheckout/{type}/{date}/{userid}", ResponseFormat = WebMessageFormat.Json)]
        Response GetCheckOut(string type, string userid, string date);
        [OperationContract]
        [WebInvoke(Method = "GET",
UriTemplate = "Access/Getlogsforentrance/{type}/{date}/{userid}", ResponseFormat = WebMessageFormat.Json)]
        Response GetLogforEntrance(string type, string userid, string date);

        [OperationContract]
        [WebInvoke(Method = "GET",
UriTemplate = "Access/Getlogsforacu/{type}/{date}/{acu}/{userid}", ResponseFormat = WebMessageFormat.Json)]
        Response GetLogforacu(string type, string acu, string userid, string date);
        [OperationContract]
        [WebInvoke(Method = "GET",
UriTemplate = "Access/Getlogsforallacu/{type}/{date}/{userid}", ResponseFormat = WebMessageFormat.Json)]
        Response GetLogforAllAcu(string type, string userid, string date);

        // TODO: Add your service operations here
    }

}
