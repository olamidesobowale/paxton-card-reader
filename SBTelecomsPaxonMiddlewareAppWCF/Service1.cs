﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SBTelecomsPaxonMiddlewareAppWCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    public class SBTelecomsACUService : ISBTelecomsACUService
    {
        
        public SBTelecomsACUService()
        {

        }

        public Response getAllUsers()
        {
            return Service.GetAllUsers();
        }

        public Response getByFilter(string filtertype, string filtervalue)
        {
            return Service.getByFilter(filtertype, filtervalue);
        }

        public Response GetCardTokenByUserId(string userid)
        {
            return Service.GetCardTokenByUserId(userid);
        }

        public Response GetCheckIn(string type, string userid, string date)
        {

            return Service.GetCheckInCheckOut(type, userid, date);

        }

        public Response GetCheckInCheckOut(string type, string userid, string date)
        {
           return Service.GetCheckInCheckOut(type, userid, date);
        }

        public Response GetCheckInCheckOutToday(string type, string userid)
        {
            return Service.GetCheckInCheckOutToday(type, userid);
        }

        public Response GetCheckInToday(string type, string userid)
        {
            return Service.GetCheckInCheckOutToday(type, userid);
        }

        public Response GetCheckOut(string type, string userid, string date)
        {
            return Service.GetCheckInCheckOut(type, userid, date);
        }

        public Response GetCheckoutToday(string type, string userid)
        {
            return Service.GetCheckInCheckOutToday(type, userid);
        }

        public Response GetLogforacu(string type, string acu, string userid, string date)
        {
            return Service.GetLogForACU(type, acu, userid, date);
        }

        public Response GetLogforAllAcu(string type, string userid, string date)
        {
            return Service.GetLogForAllACU(type, userid, date);
        }

        public Response GetLogforEntrance(string type, string userid, string date)
        {
            return Service.GetLogforEntrance(type, userid, date);
        }

        public Response GetUserByAccessToken(string Token)
        {
            return Service.GetUserByAccessToken(Token);
        }

        public Response GetUserByUserId(string userid)
        {
            return Service.GetUserByUserId(userid);
        }
    }
}
